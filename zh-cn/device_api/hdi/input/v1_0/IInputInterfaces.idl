/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup HdiInput
 * @{
 *
 * @brief Input模块向上层服务提供了统一接口。
 *
 * 上层服务开发人员可根据Input模块提供的向上统一接口获取如下能力：Input设备的打开和关闭、Input事件获取、设备信息查询、回调函数注册、特性状态控制等。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IInputInterfaces.idl
 *
 * @brief Input设备的打开和关闭、Input事件获取、设备信息查询、回调函数注册、特性状态控制等接口。
 *
 * 上层服务调用相关接口实现：Input设备的打开和关闭、Input事件获取、设备信息查询、回调函数注册、特性状态控制等功能。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief Input模块接口的包路径。
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.input.v1_0;

import ohos.hdi.input.v1_0.IInputCallback;
import ohos.hdi.input.v1_0.InputTypes;

 /**
 * @brief Input模块向上层服务提供了统一接口。
 *
 * 上层服务开发人员可根据Input模块提供的向上统一接口实现Input设备的打开和关闭、Input事件获取、设备信息查询、回调函数注册、特性状态控制等功能。
 *
 * @since 3.2
 * @version 1.0
 */
interface IInputInterfaces {
    /**
     * @brief Input服务用于扫描所有在线设备。
     *
     * @param staArr 存放Input设备扫描信息的数组，信息包含设备索引以及设备类型，具体参考{@link DevDesc}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     *
     * @since 3.2
     */
    ScanInputDevice([out] struct DevDesc[] staArr);

    /**
     * @brief Input服务打开对应设备的设备文件。
     *
     * @param devIndex Input设备索引，用于标志Input设备，取值从0开始，最多支持32个设备。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    OpenInputDevice([in] unsigned int devIndex);

    /**
     * @brief Input服务关闭对应设备的设备文件。
     *
     * @param devIndex Input设备索引，用于标志Input设备，取值从0开始，最多支持32个设备。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    CloseInputDevice([in] unsigned int devIndex);

    /**
     * @brief Input服务获取devIndex对应的Input设备信息。
     *
     * @param devIndex Input设备索引，用于标志Input设备，取值从0开始，最多支持32个设备。
     * @param devInfo 即devIndex对应的设备的设备信息，具体参考{@link DeviceInfo}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    GetInputDevice([in] unsigned int devIndex, [out] struct DeviceInfo devInfo);

    /**
     * @brief Input服务获取所有Input设备列表的设备信息。
     *
     * @param devNum 当前已经注册过的所有Input设备的总数。
     * @param devList Input设备列表所对应的设备信息，具体参考{@link DeviceInfo}。
     * @param size 即指定deviceList数组对应的元素个数。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    GetInputDeviceList([out] unsigned int devNum, [out] struct DeviceInfo[] devList, [in]unsigned int size);

    /**
     * @brief 设置devIndex对应的Input设备的电源状态。
     *
     * 在系统休眠或者唤醒时，Input服务或电源管理模块设置电源状态，使驱动IC能正常进入对应的模式。
     *
     * @param devIndex Input设备索引，用于标志Input设备，取值从0开始，最多支持32个设备。
     * @param status 设置的电源状态，如0表示正常唤醒、1表示休眠下电、2表示低功耗、3表示未知电源状态等。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    SetPowerStatus([in] unsigned int devIndex, [in] unsigned int status);

    /**
     * @brief 获取devIndex对应Input设备的电源状态。
     *
     * 在系统休眠或者唤醒时，Input服务或电源管理模块获取电源状态。
     *
     * @param devIndex Input设备索引，用于标志Input设备，取值从0开始，最多支持32个设备。
     * @param status 获取的对应设备索引的电源状态，如0表示正常唤醒、1表示休眠下电、2表示低功耗、3表示未知电源状态等。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    GetPowerStatus([in] unsigned int devIndex, [out] unsigned int status);

    /**
     * @brief 获取devIndex对应的Input设备的类型。
     *
     * @param devIndex Input设备索引，用于标志Input设备，取值从0开始，最多支持32个设备。
     * @param deviceType 获取的对应设备索引的设备类型，如0表示触屏、1表示物理按键、2表示键盘、3表示鼠标等。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    GetDeviceType([in] unsigned int devIndex, [out] unsigned int deviceType);

    /**
     * @brief 获取devIndex对应的Input设备的编码信息。
     *
     * 一款产品通常会有多家模组和Driver IC，上层应用如果关注具体器件型号，则通过此接口来获取。
     *
     * @param devIndex Input设备索引，用于标志Input设备，取值从0开始，最多支持32个设备。
     * @param chipInfo 获取的对应设备索引的器件编码信息。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2 
     * @version 1.0
     */
    GetChipInfo([in] unsigned int devIndex, [out] String chipInfo);

    /**
     * @brief 获取devIndex对应的Input设备的模组厂商名。
     *
     * @param devIndex Input设备索引，用于标志Input设备，取值从0开始，最多支持32个设备。
     * @param vendorName 获取的对应设备索引的模组厂商名。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    GetVendorName([in] unsigned int devIndex, [out] String vendorName);

    /**
     * @brief 获取devIndex对应的Input设备的驱动芯片名。
     *
     * @param devIndex Input设备索引，用于标志Input设备，取值从0开始，最多支持32个设备。
     * @param chipName 获取的对应设备索引的驱动芯片名。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    GetChipName([in] unsigned int devIndex, [out] String chipName);

    /**
     * @brief 设置devIndex对应的Input设备的手势模式。
     *
     * 上层应用开关手势模式，即设置手势模式的对应使能bit。
     *
     * @param devIndex Input设备索引，用于标志Input设备，取值从0开始，最多支持32个设备。
     * @param gestureMode 手势模式的开关状态。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    SetGestureMode([in] unsigned int devIndex, [in] unsigned int gestureMode);

    /**
     * @brief 执行容值自检测试。
     *
     * 启动不同检测场景下的容值自检测试，并获取测试结果，容值自检项由器件厂商自定义，
     * 一般包括RawData测试、短路检测、开路检测、干扰检测、行列差检测等测试项。
     *
     * @param devIndex Input设备索引，用于标志Input设备，取值从0开始，最多支持32个设备。
     * @param testType 容值测试的测试类型，如0表示基础容值测试、1表示全量容值自检测试、2表示MMI容值测试、3表示老化容值测试等。
     * @param result 容值测试的结果，成功则输出“SUCC”，失败则返回对应的错误提示。
     * @param length 保存容值测试结果的内存长度。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    RunCapacitanceTest([in] unsigned int devIndex, [in] unsigned int testType, [out] String result, [in] unsigned int length);

    /**
     * @brief 执行拓展指令。
     *
     * @param devIndex Input设备索引，用于标志Input设备，取值从0开始，最多支持32个设备。
     * @param cmd 拓展指令数据包，包括指令编码及参数，具体参考{@link ExtraCmd}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    RunExtraCommand([in] unsigned int devIndex, [in] struct ExtraCmd cmd);

    /**
     * @brief 注册devIndex对应的Input设备的回调函数。
     *
     * Input服务通过此接口注册数据回调函数到HDI中，HDI通过此回调函数上报Input事件。
     *
     * @param devIndex Input设备索引，用于标志Input设备，取值从0开始，最多支持32个设备。
     * @param eventPkgCallback 回调函数的函数指针。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    RegisterReportCallback([in] unsigned int devIndex, [in] IInputCallback eventPkgCallback);

    /**
     * @brief 注销devIndex对应的Input设备的回调函数。
     *
     * @param devIndex Input设备索引，用于标志Input设备，取值从0开始，最多支持32个设备。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    UnregisterReportCallback([in] unsigned int devIndex);

    /**
     * @brief 注册Input设备的热插拔回调函数。
     *
     * Input服务通过此接口注册回调函数到HDI中，所有Input设备由此函数进行热插拔事件上报。
     *
     * @param hotPlugCallback 回调函数的函数指针。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    RegisterHotPlugCallback([in] IInputCallback hotPlugCallback);

    /**
     * @brief 注销Input设备的热插拔回调函数。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    UnregisterHotPlugCallback();
}
/** @} */
