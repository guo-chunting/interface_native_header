/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Audio
 * @{
 *
 * @brief Audio模块接口定义
 *
 * 音频接口涉及自定义类型、驱动加载接口、驱动适配器接口、音频播放（render）接口、音频录音（capture）接口等
 *
 * @since 1.0
 * @version 1.0
 */

/**
 * @file audio_volume.h
 *
 * @brief Audio音量的接口定义文件
 *
 * @since 1.0
 * @version 1.0
 */


#ifndef AUDIO_VOLUME_H
#define AUDIO_VOLUME_H

#include "audio_types.h"

/**
 * @brief AudioVolume音频音量接口
 *
 * 提供音频播放（render）或录音（capture）需要的公共音量驱动能力，包括静音操作、设置音量、设置增益等
 *
 * @since 1.0
 * @version 1.0
 */
struct AudioVolume {
    /**
     * @brief 设置音频的静音状态
     *
     * @param handle 待操作的音频句柄
     * @param mute 待设置的静音状态，true表示静音操作、false表示取消静音操作
     * @return 成功返回值0，失败返回负值
     * @see GetMute
     */
    int32_t (*SetMute)(AudioHandle handle, bool mute);

    /**
     * @brief 获取音频的静音状态
     *
     * @param handle 待操作的音频句柄
     * @param mute 获取的静音状态保存到mute中，true表示静音操作、false表示取消静音操作
     * @return 成功返回值0，失败返回负值
     * @see SetMute
     */
    int32_t (*GetMute)(AudioHandle handle, bool *mute);

    /**
     * @brief 设置一个音频流的音量
     *
     * 音量的取值范围是0.0~1.0，如果音频服务中的音量等级为15级（0 ~ 15），
     * 则音量的映射关系为0.0表示静音，1.0表示最大音量等级（15）
     *
     * @param handle 待操作的音频句柄
     * @param volume 待设置的音量，范围0.0~1.0
     * @return 成功返回值0，失败返回负值
     */
    int32_t (*SetVolume)(AudioHandle handle, float volume);

    /**
     * @brief 获取一个音频流的音量
     *
     * @param handle 待操作的音频句柄
     * @param volume 获取的音量保存到volume中，范围0.0~1.0
     * @return 成功返回值0，失败返回负值
     * @see SetVolume
     */
    int32_t (*GetVolume)(AudioHandle handle, float *volume);

    /**
     * @brief 获取音频流增益的阈值
     *
     * 在具体的功能实现中，可以根据芯片平台的实际情况来进行处理：
     * <ul>
     *   <li>1. 可以使用实际的增益值，例如增益的范围为-50db ~ 6db</li>
     *   <li>2. 也可以将增益范围设定为0.0~1.0，如果增益的范围为-50db ~ 6db，
     *          则增益的映射关系为0.0表示静音，1.0表示最大增益（6db）</li>
     * </ul>
     * @param handle 待操作的音频句柄
     * @param min 获取的音频增益的阈值下限保存到min中
     * @param max 获取的音频增益的阈值上限保存到max中
     * @return 成功返回值0，失败返回负值
     * @see GetGain
     * @see SetGain
     */
    int32_t (*GetGainThreshold)(AudioHandle handle, float *min, float *max);

    /**
     * @brief 获取音频流的增益
     *
     * @param handle 待操作的音频句柄
     * @param gain 保存当前获取到的增益到gain中
     * @return 成功返回值0，失败返回负值
     * @see GetGainThreshold
     * @see SetGain
     */
    int32_t (*GetGain)(AudioHandle handle, float *gain);

    /**
     * @brief 设置音频流的增益
     *
     * @param handle 待操作的音频句柄
     * @param gain gain 待设置的增益
     * @return 成功返回值0，失败返回负值
     * @see GetGainThreshold
     * @see GetGain
     */
    int32_t (*SetGain)(AudioHandle handle, float gain);
};

#endif /* AUDIO_VOLUME_H */
/** @} */
