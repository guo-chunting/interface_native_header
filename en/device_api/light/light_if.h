/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Light
 * @{
 *
 * @brief Provides APIs for the light service to access the light driver.
 *
 * 
 * After obtaining the light driver object or proxy, the light service can call related APIs to obtain light information, turn on or off a light, and set the light blinking mode based on the light ID.
 *
 * @since 3.1
 */

/**
 * @file Light_if.h
 *
 * @brief Declares common API of the light module. These APIs can be used to obtain the light ID, turn on or off the light, and set the light brightness and blinking mode.
 * @since 3.1
 */

#ifndef LIGHT_IF_H
#define LIGHT_IF_H

#include <stdint.h>
#include "light_type.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

/**
 * @brief Defines the basic operations that can be performed on lights.
 *
 * The operations include obtaining light information, turning on or off a light, and setting the light brightness and blinking mode.
 */

struct LightInterface {
    /**
      * @brief Obtains information about all types of lights in the system.
     *
     * @param lightInfo Indicates the double pointer to the light information. For details, see {@link LightInfo}.
     * @param count Indicates the pointer to the number of lights.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a negative value if the operation fails.
     *
     * @since 3.1
     */
    int32_t (*GetLightInfo)([out] struct LightInfo **lightInfo, [out] uint32_t *count);

    /**
     * @brief Turns on available lights in the list based on the specified light ID.
     *
     * @param lightId Indicates the light ID. For details, see {@link LightId}.
     * @param effect Indicates the pointer to the light effect. If <b>lightbrightness</b> is <b>0</b>,
     * the default brightness set in the HCS will be used. For details, see {@link LightEffect}.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns <b>-1</b> if the light ID is not supported.
     * @return Returns <b>-2</b> if the blinking setting is not supported.
     * @return Returns <b>-3</b> if the light brightness cannot be set.
     *
     * @since 3.1
     */
    int32_t (*TurnOnLight)([in] uint32_t lightId, [in] struct LightEffect *effect);

    /**
     * @brief Turns off available lights in the list based on the specified light ID.
     *
     * @param lightId Indicates the light ID. For details, see {@link LightId}.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a negative value if the operation fails.
     *
     * @since 3.1
     */
    int32_t (*TurnOffLight)([in] uint32_t lightId);
};

/**
 * @brief Create a <b>LightInterface</b> instance.
 *
 * The created instance can be used to perform light control operations.
 *
 * @return Returns <b>0</b> if the operation is successful.
 * @return Returns a negative value if the operation fails.
 *
 * @since 3.1
 */
const struct LightInterface *NewLightInterfaceInstance(void);

/**
 * @brief Releases the <b>LightInterface</b> instance and related resources.
 *
 * @return Returns <b>0</b> if the operation is successful.
 * @return Returns a negative value if the operation fails.
 *
 * @since 3.1
 */
int32_t FreeLightInterfaceInstance(void);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */

#endif /* LIGHT_IF_H */
/** @} */
